const groceryList = [
  "oeuf",
  "Lait",
  "jambon"
]

export function getDownload(req, res) {
  const id = req.params.id
  res.send(id)
}

export function putGroceryList(req, res) {
  const id = req.params.id || null
  const name = req.body.name || "Random"
  const maxGroceryList = groceryList.length;

  if(id && id < maxGroceryList) {
    console.log(`update value ${groceryList[id]} as ${name}`)
    groceryList[id] = name;
    res.send(groceryList);
    return;
  }

  groceryList.push(name);
  res.send(groceryList)
}

export function deleteGroceryList(req, res) {
  const id = req.params.id || null
  const maxGroceryList = groceryList.length;

  if(!id || id >= maxGroceryList) {
    res.status(404).send(groceryList);
    return;
  }

  let removed = groceryList.splice(id, 1);
  res.send(groceryList)
}