import express from "express"
import bodyParser from 'body-parser'
import cors from 'cors'
import routes from './router.js'

const app = express();
const PORT = 3000;

app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json());

app.get('/', (req, res) => {
  const mx = req.params;
  console.log(mx);
  res.send("Hello world");
});

app.get('/games', (req, res) => {
  const mx = req.params;
  console.log(mx);
  res.send("Hello world");
});

app.get('/game/:id', (req, res) => {
  const mx = req.params;
  console.log(mx);
  res.send("Hello world");
});

app.put('/game', (req, res) => {
  const mx = req.params;
  console.log(mx);
  res.send("Hello world");
});

app.get('/gamee/:id/comments', (req, res) => {
  const mx = req.params;
  console.log(mx);
  res.send("Hello world");
});

app.put('/game/:id/comment', (req, res) => {
  const mx = req.params;
  console.log(mx);
  res.send("Hello world");
});

app.get('/games/:id/comment/:id', (req, res) => {
  const mx = req.params;
  console.log(mx);
  res.send("Hello world");
});

//app.get('/article/:id', (req, res) => {
//  const params = req.params;
//  res.send(params);
//});

//app.get('/article', (req, res) => {
//  const params = req.quey;
//  res.send(params);
//});

routes(app);

//app.post('/truc', (req, res) => {
//  const m = req.body;
//  console.log(m);
//  res.send(m);
//});

app.use((req, res) => {
  res.status(404).send({url: req.originalUrl + ' not found'})
});

app.listen(PORT, () =>
  console.log(`Node server running on ${PORT}!`),
);